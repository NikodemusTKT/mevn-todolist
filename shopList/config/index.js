module.exports = {
  dev: {
    proxyTable: {
      '/api/todos': {
        target: 'http://localhost:4000',
        changeOrigin: true
      }
    }

  }
}