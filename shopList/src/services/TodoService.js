import axios from 'axios';
const url = process.env.API_URL || 'http://localhost:4000/api/todos/'

export default {
  async fetchTodos() {
    return await axios.get(url);
  },
  async addTodo(todo) {
    return await axios.post(url, {name: todo, done: false});
  },
  async deleteTodo(id) {
    return await axios.delete(url+id)
  },
  async updateTodo(params)  {
    return await axios.put(url+params.id, params);
  }
}