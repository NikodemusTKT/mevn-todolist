'use strict'
var mongoose = require('mongoose');

var todo = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  done: {
    type: Boolean,
    default: false
  },
  updateTime: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Todo',todo);