module.exports = {
  DB: process.env.DB || 'mongodb://mongo:27017/todolist',
  APP_PORT: process.env.API_PORT || 4000
};