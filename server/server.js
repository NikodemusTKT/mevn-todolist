'use strict'

var express = require('express');
var cors = require('cors');
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');

var todoRouter = require('./routes/todos');

// Database connection
var config = require('./Config');
mongoose.Promise = global.Promise;
mongoose.connect(config.DB || "mongodb://localhost:27017/todolist", {useNewUrlParser: true});

var app = express();

// Middlewares

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Static public folder for static elements
app.use(express.static(path.join(__dirname,'public')));

// Express server
const port = config.APP_PORT;
app.listen(port,() => {
  console.log(`Express listening on PORT ${port}`);
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// Routes
// Use api route for handling todo items
app.use('/api/todos', todoRouter);

// Serve index.html from the root request
app.get("/",(req,res,next) => {
  res.sendFile('./public/index.html');
})

