'use strict'

var express = require('express');
var todoRouter = express.Router();
var Todo = require('../models/Todo');

// get all todo items
todoRouter.get('/', async (req,res,next) => {
  try {
  let todos = await Todo.find({});
  res.json(todos);
  } catch(err) {
      return next(new Error("No any tasks found."));
  }
})

// create new todo item
todoRouter.post("/", async (req,res) => {
  try {
    let todo = await Todo.create({
      name: req.body.name,
      done: req.body.done
    });
    res.status(200).json(todo);
  } catch(err) {
      res.status(400).send('Unable to add a new todo item');
  }
});

// delete a todo item
todoRouter.delete('/:id', async (req,res,next) => {
  try {
  let todo = await Todo.findByIdAndDelete(req.params.id);
  if (!todo) {
      return next(new Error('Todo was not found'));
  } else {
    res.status(204).send({message: 'Todo succefully deleted', id:todo._id});
  }
} catch(err) {
  return res.status(500).send(new Error(err));
}
});

// Update a todo item
todoRouter.put("/:id", async (req,res,next) => {
  try {
    let todo = await Todo.findByIdAndUpdate(req.params.id,req.body,{new: true});
    if (!todo) {
      return res.status(400).send("Could not found item with given id");
    } else {
      res.status(200).json(todo);
    }
  } catch (err) {
    res.status(500).send("testy");
  }
  });

module.exports = todoRouter;


